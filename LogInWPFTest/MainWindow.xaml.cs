﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using LoggerLib;



namespace LogInWPFTest
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        private static readonly Logger logger = new Logger();//typeof declares in what class will be caled the logger

        public MainWindow()
        {
            InitializeComponent();
            logger.log.Info("Form has initialized");
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            logger.log.Info("Button has pressed");//just test
        }

        private void Bootstrapper()
        {

        }
    }
}
