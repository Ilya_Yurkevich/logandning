﻿using log4net;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;
using log4net.Core;
using Ninject;
using LoggerLib;

namespace LogInWPFTest
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        //private static readonly ILog log = LogManager.GetLogger(typeof(App));
        protected override void OnStartup(StartupEventArgs e)
        {
            IKernel kernel = new StandardKernel();
            kernel.Bind<Ilogger>().To<Logger>();

            
            base.OnStartup(e);
        }

        public void Message()
        {
            Ilogger lg ;
            lg.throwMessage();
            lg.GetLogger().log.Info("message test");
        }

        protected override void OnExit(ExitEventArgs e)
        {
            base.OnExit(e);
        }

    }
}
