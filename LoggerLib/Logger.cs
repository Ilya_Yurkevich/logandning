﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using log4net;

namespace LoggerLib
{
    public class Logger:Ilogger
    {
        
        public readonly ILog log;
        public Logger()
        {
            log = LogManager.GetLogger(typeof(Logger));
        }

        public Logger GetLogger()
        {
            return this;
        }

        public void throwMessage()
        {
            log.Info("message");
        }
    }
}
